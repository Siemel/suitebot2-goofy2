package suitebot2.ai;

import org.junit.Assert;
import org.junit.Test;
import suitebot2.game.GamePlan;
import suitebot2.game.GameRound;
import suitebot2.game.GameSetup;
import suitebot2.game.Player;
import suitebot2.game.PlayerMove;
import suitebot2.game.Point;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;

/**
 * TODO: Enter a paragraph that summarizes what the class does and why someone might want to utilize it
 *
 * Copyright © 2015, NetSuite, Inc.
 *
 * @author snaran
 * @version 2016.2
 * @since 2016-06-23
 */
public class GoofyTest extends Assert
{
    @Test
    public void basicTest() throws Exception
    {
        GoofyBotAi bot = new GoofyBotAi();
        GameSetup gameSetup = new GameSetup(
                1,
                Arrays.asList(new Player(2, "2"), new Player(1, "1")),
                new GamePlan(5, 7, Arrays.asList(new Point(0, 0), new Point(3,3)), 3)
        );
        String move = bot.initializeAndMakeMove(gameSetup);
        System.out.println(move + " " + bot.positions[0] + ' ' + bot.positions[1]);

        move = bot.makeMove(new GameRound(Arrays.asList(new PlayerMove(1, move), new PlayerMove(2, "U"))));
        System.out.println(move + " " + bot.positions[0] + ' ' + bot.positions[1]);

        move = bot.makeMove(new GameRound(Arrays.asList(new PlayerMove(1, move), new PlayerMove(2, "L"))));
        System.out.println(move + " " + bot.positions[0] + ' ' + bot.positions[1]);

    }

    @Test
    public void runTest() throws Exception
    {
        final int numPlayers = 6;

        List<Player> players = new ArrayList<>(numPlayers);
        List<Point> startingPositions = new ArrayList<>(numPlayers);
        for (int i=0; i<numPlayers; i++)
        {
            players.add(new Player(i, String.valueOf(i)));
            int x = (int) (Math.random()*100);
            int y = (int) (Math.random()*100);
            startingPositions.add(new Point(x,y));
            //startingPositions.add(new Point(i+5,1));
        }

        GameSetup[] gameSetups = new GameSetup[numPlayers];
        for (int i=0; i<numPlayers; i++)
        {
            gameSetups[i] =
                    new GameSetup(
                            i,
                            players,
                            new GamePlan(100, 100, startingPositions, 5)
                    );
        }


        GoofyBotAi[] bots = new GoofyBotAi[numPlayers];
        for (int i=0; i<numPlayers; i++)
        {
            bots[i] = new GoofyBotAi();
        }

        String moves[] = new String[numPlayers];
        for (int i=0; i<numPlayers; i++)
        {
            moves[i] = bots[i].initializeAndMakeMove(gameSetups[i]);
        }
        print(bots[0], moves);

        for (int j=0; j<200; j++)
        {
            List<PlayerMove> lastMoves = new ArrayList<>();
            for (int i=0; i<numPlayers; i++)
            {
                lastMoves.add(new PlayerMove(i, moves[i]));
            }
            for (int i=0; i<numPlayers; i++)
            {
                moves[i] = bots[i].makeMove(new GameRound(lastMoves));
            }
            print(bots[0], moves);
        }
    }

    private void print(GoofyBotAi bot, String[] moves)
    {
        for (int i=0; i<bot.gameSetup.players.size(); i++)
        {
            System.out.print("(" + bot.positions[i].x + ',' + bot.positions[i].y + ',' + moves[i] + ") ");
        }
        System.out.println();
    }
}
