package suitebot2.json;

import org.junit.Test;
import suitebot2.game.GameRound;
import suitebot2.game.GameSetup;
import suitebot2.game.Point;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class JsonUtilTest
{
	public static String SETUP_MESSAGE =
			"{" +
					"\"messageType\":\"setup\"," +
					"\"aiPlayerId\":1," +
					"\"players\":[{\"id\":2,\"name\":\"R2D2\"},{\"id\":1,\"name\":\"C-3PO\"}]," +
					"\"gamePlan\":{" +
						"\"width\":10," +
						"\"height\":20," +
						"\"startingPositions\":[{\"x\":2,\"y\":2},{\"x\":7,\"y\":7}]," +
						"\"maxRounds\":25" +
					"}" +
			"}";

	public static String MOVES_MESSAGE =
			"{" +
					"\"messageType\":\"moves\"," +
					"\"moves\":[{\"playerId\":1,\"move\":\"FIRE\"},{\"playerId\":2,\"move\":\"JUMP\"}]" +
			"}";

	@Test
	public void testDecodeMessageType() throws Exception
	{
		assertThat(JsonUtil.decodeMessageType(SETUP_MESSAGE), is(JsonUtil.MessageType.SETUP));
		assertThat(JsonUtil.decodeMessageType(MOVES_MESSAGE), is(JsonUtil.MessageType.MOVES));
	}

	@Test
	public void testDecodeMovesMessage() throws Exception
	{
		GameRound gameRound = JsonUtil.decodeMovesMessage(MOVES_MESSAGE);
		assertThat(gameRound.moves.size(), is(2));
		assertThat(gameRound.moves.get(0).playerId, is(1));
		assertThat(gameRound.moves.get(0).move, is("FIRE"));
		assertThat(gameRound.moves.get(1).playerId, is(2));
		assertThat(gameRound.moves.get(1).move, is("JUMP"));
	}

	@Test
	public void testDecodeSetupMessage() throws Exception
	{
		GameSetup gameSetup = JsonUtil.decodeSetupMessage(SETUP_MESSAGE);
		assertThat(gameSetup.aiPlayerId, is(1));
		assertThat(gameSetup.players.get(0).id, is(2));
		assertThat(gameSetup.players.get(0).name, is("R2D2"));
		assertThat(gameSetup.players.get(1).id, is(1));
		assertThat(gameSetup.players.get(1).name, is("C-3PO"));
		assertThat(gameSetup.gamePlan.width, is(10));
		assertThat(gameSetup.gamePlan.height, is(20));
		assertThat(gameSetup.gamePlan.startingPositions.get(0), is(new Point(2, 2)));
		assertThat(gameSetup.gamePlan.startingPositions.get(1), is(new Point(7, 7)));
		assertThat(gameSetup.gamePlan.maxRounds, is(25));
	}
}