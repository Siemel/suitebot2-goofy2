package suitebot2.game;

import com.google.common.collect.ImmutableList;

import java.util.Collection;
import java.util.List;

public class GameRound
{
	public final List<PlayerMove> moves;

	public GameRound(Collection<PlayerMove> moves)
	{
		this.moves = ImmutableList.copyOf(moves);
	}

	@Override
	public String toString()
	{
		return "GameRound{" +
				       "moves=" + moves +
				       '}';
	}

	public String getPlayerMove(int playerId)
	{
		for (PlayerMove move : moves)
		{
			if (move.playerId == playerId) return move.move;
		}
		return "X";
	}
}
