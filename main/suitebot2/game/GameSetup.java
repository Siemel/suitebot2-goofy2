package suitebot2.game;

import java.util.List;

public class GameSetup
{
	public final int aiPlayerId;
	public final List<Player> players;
	public final GamePlan gamePlan;

	public GameSetup(int aiPlayerId, List<Player> players, GamePlan gamePlan)
	{
		this.aiPlayerId = aiPlayerId;
		this.players = players;
		this.gamePlan = gamePlan;
	}

	@Override
	public String toString()
	{
		return "GameSetup{" +
				       "aiPlayerId=" + aiPlayerId +
				       ", players=" + players +
				       ", gamePlan=" + gamePlan +
				       '}';
	}

	public Point getMyPosition()
	{
		int playerIndex = players.indexOf(new Player(aiPlayerId, null));
		return gamePlan.startingPositions.get(playerIndex);
	}

	public int getMyIndex()
	{
		for (int i=0; i<players.size(); i++)
		{
			Player player = players.get(i);
			if (player.id == aiPlayerId) return i;
		}
		return 0;
	}
}
