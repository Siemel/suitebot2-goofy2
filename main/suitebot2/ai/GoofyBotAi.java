package suitebot2.ai;

import suitebot2.game.GameRound;
import suitebot2.game.GameSetup;
import suitebot2.game.Player;
import suitebot2.game.Point;

import java.util.ArrayList;
import java.util.List;

/**
 * TODO: Enter a paragraph that summarizes what the class does and why someone might want to utilize it
 *
 * <p>Copyright © 2000-2016, NetSuite, Inc.</p>
 *
 * @author byang
 * @version 2016.2
 * @since 2016-06-22
 */
public class GoofyBotAi implements BotAi
{
    public GameSetup gameSetup;
    private int myIndex;
    public Point[] positions;
    private Player[][] grid;
    Point lastPosition;
    Point secondLastPosition;

    @Override
    public String initializeAndMakeMove(GameSetup gameSetup)
    {
        this.gameSetup = gameSetup;
        myIndex = gameSetup.getMyIndex();
        positions = new Point[gameSetup.players.size()];
        grid = new Player[gameSetup.gamePlan.width][gameSetup.gamePlan.height];
        for (int i=0; i<gameSetup.players.size(); i++)
        {
            Player player = gameSetup.players.get(i);
            Point position = gameSetup.gamePlan.startingPositions.get(i);
            positions[i] = position.clone();
            grid[position.x][position.y] = player;
        }
        lastPosition = positions[myIndex];
        secondLastPosition = null;
        return doMakeMove();
    }

    @Override
    public String makeMove(GameRound gameRound)
    {
        // update all players current position and grid
        for (int i = 0; i < gameSetup.players.size(); i++)
        {
            Player player = gameSetup.players.get(i);
            String move = gameRound.getPlayerMove(player.id);
            Point position = move(positions[i], move);
            positions[i] = position;
            Player playerAtPoint = grid[position.x][position.y];
            if (playerAtPoint == null)
            {
                grid[position.x][position.y] = player;
            }
        }

        // update last position fields
        secondLastPosition = lastPosition;
        lastPosition = positions[myIndex];

        return doMakeMove();
    }

    private String doMakeMove()
    {
        // find blank rows of all directions
        List<Counter> counters = new ArrayList<>(4);
        counters.add(new Counter(countBlank(true, -1), "L"));
        counters.add(new Counter(countBlank(true, +1), "R"));
        counters.add(new Counter(countBlank(false, -1), "U"));
        counters.add(new Counter(countBlank(false, +1), "D"));
        counters.sort(new Counter.Compare());

        // find direction with most blank rows
        int maxCount = counters.get(0).count;
        List<Counter> bestCounters = new ArrayList<>(4);
        bestCounters.add(counters.get(0));
        for (int i=1; i<counters.size(); i++)
        {
            Counter counter = counters.get(i);
            if (counter.count < maxCount)
                break;
            bestCounters.add(counter);
        }

        String move = "U";

        // from the best directions pick one that is not occupied
        for (Counter counter : bestCounters)
        {
            move = counter.move;
            Point newPosition = move(positions[myIndex], move);
            if (grid[newPosition.x][newPosition.y] == null)
                break;
        }

        // if our new position is same as second last position then pick a random move
        Point newPosition = move(positions[myIndex], move);
        if (newPosition.equals(secondLastPosition))
        {
            List<String> possibleMoves = new ArrayList<>();
            possibleMoves.add("U");
            possibleMoves.add("D");
            possibleMoves.add("L");
            possibleMoves.add("R");
            while (!possibleMoves.isEmpty())
            {
                move = getRandomMove(possibleMoves);
                newPosition = move(positions[myIndex], move);
                if (grid[newPosition.x][newPosition.y] == null)
                    break;
                possibleMoves.remove(move);
            }
        }

        return move;
    }

    public Point move(Point point, String move)
    {
        if (move.equalsIgnoreCase("L")) return new Point(point.x-1 < 0 ? gameSetup.gamePlan.width - 1 : point.x - 1, point.y);
        if (move.equalsIgnoreCase("R")) return new Point(point.x+1 >= gameSetup.gamePlan.width ? 0 : point.x + 1, point.y);
        if (move.equalsIgnoreCase("U")) return new Point(point.x, point.y-1 < 0 ? gameSetup.gamePlan.height - 1 : point.y-1);
        if (move.equalsIgnoreCase("D")) return new Point(point.x, point.y+1 >= gameSetup.gamePlan.height ? 0 : point.y+1);
        return point.clone();
    }

    private String makeRandomMove()
    {
        List<String> possibleMoves = new ArrayList<>();
        possibleMoves.add("U");
        possibleMoves.add("D");
        possibleMoves.add("L");
        possibleMoves.add("R");
        return getRandomMove(possibleMoves);
    }

    private static String getRandomMove(List<String> possible)
    {
        int index = (int) (Math.random()*possible.size());
        return possible.get(index);
    }

    private static final int DIRECTION_COUNT = 5;
    private static final int NON_DIRECTION_COUNT = 3;

    private int countBlank(boolean xDirection, int sign)
    {
        boolean yDirection = !xDirection;
        int xSign = xDirection ? sign : 1;
        int ySign = yDirection ? sign : 1;
        int count = 0;
        Point position = positions[myIndex];
        for (int xcount=(xDirection?1:-NON_DIRECTION_COUNT); xcount<=(xDirection?+DIRECTION_COUNT:NON_DIRECTION_COUNT); xcount++)
        {
            int x = position.x + xSign*xcount;
            if (x < 0) x = gameSetup.gamePlan.width + x;
            if (x >= gameSetup.gamePlan.width) x = x - gameSetup.gamePlan.width;

            for (int ycount=(yDirection?1:-NON_DIRECTION_COUNT); ycount<=(yDirection?DIRECTION_COUNT:+NON_DIRECTION_COUNT); ycount++)
            {
                int y = position.y + ySign*ycount;
                if (y < 0) y = gameSetup.gamePlan.height + y;
                if (y >= gameSetup.gamePlan.height) y = y - gameSetup.gamePlan.height;

                if (grid[x][y] == null)
                    count++;
            }
        }
        return count;
    }

    private static class Counter
    {
        private int count;
        private String move;

        Counter(int count, String move)
        {
            this.count = count;
            this.move = move;
        }

        public static class Compare implements java.util.Comparator<Counter>
        {
            @Override
            public int compare(Counter lhs, Counter rhs)
            {
                return rhs.count - lhs.count;
            }
        }
    }
}
