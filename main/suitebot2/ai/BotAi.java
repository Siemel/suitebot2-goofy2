package suitebot2.ai;

import suitebot2.game.GameRound;
import suitebot2.game.GameSetup;

public interface BotAi
{
	/**
	 * This method is called at the beginning of the game.
	 * @param gameSetup details of the game, such as the game plan dimensions, the list of players, etc
	 * @return the first move to play
	 */
	String initializeAndMakeMove(GameSetup gameSetup);

	/**
	 * This method is called in each round, except for the first one.
	 * @param gameRound all players' moves in the previous round
	 * @return the move to play
	 */
	String makeMove(GameRound gameRound);
}
